# EPD display tests
This repository aims to test the [EPDiy](https://github.com/vroland/epdiy) display driver, related libraries and experimental rendering primitives.

It is based on the [ESP-IDF](https://www.espressif.com/en/products/sdks/esp-idf) framework ([github](https://github.com/espressif/esp-idf)).

## Usage
In order to run and verify the tests, you have to:
1. [Install ESP-IDF](https://docs.espressif.com/projects/esp-idf/en/stable/esp32/get-started/index.html#setting-up-development-environment) on your machine
2. Clone this repository and its submodules:
   ```
    git clone --recurse-submodules https://codeberg.org/jdoubleu/epd_display_test.git
   ```
3. Configure your [display and board type](https://epdiy.readthedocs.io/en/latest/getting_started.html#selecting-a-display-type) in the menuconfig under `Component config -> E-Paper driver -> Display Type`: 
   ```
   idf.py menuconfig
   ```
4. Build and flash this project:
   ```
   idf.py -p <EPDIY_PORT> flash 
   ```
   
## Tests
This lists all tests conducted.

### Partial update at 0,0
Whenever the display is rotated (e.g. `EPD_ROT_PORTRAIT`) and you try to partially update any area, using `epd_hl_update_area` where either one of `x` or `y` is `0`, nothing gets updated at all.

To confirm this, just run the [`test_rotated_update_0.c`](./main/test_rotated_update_0.c) (enabled by default) and verify, that the text _"Hello from the Border"_ is not visible. Now change the area's `x` position to `1` (or anything other than `0`) [here]((https://codeberg.org/jdoubleu/epd_display_test/src/branch/main/main/test_rotated_update_0.c#L7)) and re-run the test. You should be able to see the text now!

This issue is being addressed in: https://github.com/vroland/epdiy/pull/107

### Partial area update
**Update:** This issue seems to be mostly fixed in the latest master at [`b9fc186`](https://github.com/vroland/epdiy/commit/b9fc18673d86193f5cf4c6c1719641e24d61837d#diff-f61f1ec4c5778b158ca538841ae200ff929d1e9fc7111ac183c5145c392fae3eR135-R145) (see [discussion](https://epdiy.slack.com/archives/C01L69Z16E7/p1630767636102900))

This test reproduces an issue with partial area update ([`epd_hl_update_area`](https://epdiy.readthedocs.io/en/latest/api.html#c.epd_hl_update_area)) in rotated mode.
The test can be found in [`test_update_area.c`](./main/test_update_area.c). First, it sets the display rotation to `EPD_ROT_PORTRAIT`.
It then writes a 3 digit number and a rectangle to the display. Each [`display_draw`](https://codeberg.org/jdoubleu/epd_display_test/src/branch/main/main/display.c#L23-L32) only prints the given (i.e. changed) area from the _front buffer_ to the display. After that, it immediately clears that area in the _front buffer_ by setting every pixel to white (`0xFF`).

The next write of the number results in overlapping digits/characters on the display (see below), at least in _portrait_ orientation. The same test in _landscape_ works perfectly fine (try changing the orientation [here](https://codeberg.org/jdoubleu/epd_display_test/src/branch/main/main/test_update_area.c#L72)).

| Portrait | Landscape |
| --- | --- |
| ![bug in portrait](./docs/images/test_update_area_1_portait.jpg) | ![works in landscape](./docs/images/test_update_area_1_landscape.jpg) |
| actual | expected |

### Broken stairs
> **NOTE:** It seems like this issue depends on the display being used. The ED097OC4 should display this example correctly.

When drawing horizontal lines along an imaginary diagonal line, thus producing stairs, some of those are weirdly off-set (see screenshot below). This might just be an issue with my board and display in particular.

To run this example, comment out the other tests in the [`app_main`](https://codeberg.org/jdoubleu/epd_display_test/src/branch/main/main/main.c#L13) and uncomment the `test_stairs()` function call.

![broken stairs](./docs/images/test_stairs.jpg)

### Grid
> **NOTE:** It seems like this issue depends on the display being used. The ED097OC4 should display this example correctly.

This test renders a grid with adjustable gutter spacing and additional information. By default, the gutter size is 80px and each cell displays its `x` and `y` index, as well as its absolute `x` and `y` position.

![grid](./docs/images/test_grid.jpg)

#### Defects
For some, not yet confirmed, reason, the 10th horizontal line is not printed. At a height of `800px` the line is simply skipped. It may have something to do with the orientation and internal rotation, although the display width is `825px` and not `800px`. This behaviour can also be observed in different orientations (e.g. `EPD_ROT_LADNSCAPE`), where instead of the 10th row the 10th column isn't displayed.

![grid_defect](./docs/images/test_grid_defect.jpg)

## License
This project is licensed under [GPLv3](./LICENSE).

* The `epd_driver` is licensed under [GPLv3](https://github.com/vroland/epdiy#licenses)
* JetBrains Mono, the font used, is licensed under [SIL Open Font License 1.1](https://www.jetbrains.com/lp/mono/#license) ([see license](https://github.com/JetBrains/JetBrainsMono/blob/master/OFL.txt))

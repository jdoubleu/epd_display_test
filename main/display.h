#include <epd_driver.h>
#include <epd_highlevel.h>

extern EpdiyHighlevelState g_display;

void display_setup();
void display_fullclear();
void display_draw(EpdRect area);
void display_draw_full();

void draw_border_rect(EpdRect area, int border_thickness);

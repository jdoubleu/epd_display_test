#include "display.h"

#include "fonts.h"

void write_text()
{
    const int start_x = 0;
    const int start_y = 4 * 48;

    int x = start_x;
    int y = start_y;
    epd_write_default(&JetBrainsMonoBold48, "Hello from the\nBorder", &x, &y, epd_hl_get_framebuffer(&g_display));

    EpdRect area;
    area.width = x - start_x;
    area.height = y - start_y;
    area.x = start_x;
    area.y = start_y - (area.height / 2);

    epd_draw_rect(area, 0x00, epd_hl_get_framebuffer(&g_display));

    display_draw(area);
}

void test_rotated_update_0()
{
    // pre-condition
    epd_set_rotation(EPD_ROT_PORTRAIT);

    write_text();
}

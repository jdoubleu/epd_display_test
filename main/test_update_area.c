#include "display.h"
#include <stdio.h>
#include <limits.h>

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <esp_log.h>

#include "fonts.h"

#define MIN(a,b) (a) < (b) ? (a) : (b)
#define MAX(a,b) (a) > (b) ? (a) : (b)

static const char* TAG = "UpdateArea";

/*
 * Keep the last updated area, so that the next update can take it into account,
 * and clear any artifacts from the previous update.
 *
 * Initializes it to an invalid area for the initial/first update (see below).
 */
EpdRect previous = {
    .x = INT_MAX,
    .y = INT_MAX,
    .width = INT_MIN,
    .height = INT_MIN
};

void write_num(int num)
{
    char str[100];
    snprintf(str, 100, "%d", num);

    const int pos_x = epd_rotated_display_width() / 2 - 48;
    const int pos_y = epd_rotated_display_height() / 2;

    const EpdFontProperties props = epd_font_properties_default();

    int x = pos_x;
    int y = pos_y;
    int x1, y1, w, h;
    epd_get_text_bounds(&JetBrainsMonoBold48, str, &x, &y, &x1, &y1, &w, &h, &props);
    ESP_LOGD(TAG, "About to write text \"%s\" at (x=%d, y=%d) [width=%d, height=%d]",
        str, x, y, w, h);

    EpdRect area;
    area.x = pos_x;
    area.y = pos_y - h;
    area.width = w;
    area.height = h;

    x = pos_x;
    y = pos_y;
    epd_write_default(&JetBrainsMonoBold48, str, &x, &y, epd_hl_get_framebuffer(&g_display));
    ESP_LOGD(TAG, "Text \"%s\" actually written to (x=%d, y=%d), estimated: (x=%d, y=%d)",
        str, x, y, pos_x + w, pos_y + h);

    // make sure, we update a big enough area to remove artifacts from last update
    const EpdRect intermediate = {
        .x = MIN(previous.x, area.x),
        .y = MIN(previous.y, area.y),
        .width = MAX(previous.width, area.width),
        .height = MAX(previous.height, area.height)
    };

    display_draw(intermediate);
    previous = area;
}

void print_block()
{
    EpdRect area;
    area.width = 4 * 48;
    area.height = 2 * 48;
    area.x = epd_rotated_display_width() / 2 - 48;
    area.y = epd_rotated_display_height() / 2 - (2 * area.height);

    draw_border_rect(area, 5);

    display_draw(area);
}

void run_test(void* param)
{
    for (int i = 100;; ++i)
    {
        write_num(i);
        vTaskDelay(500 / portTICK_RATE_MS);
        print_block();

        vTaskDelay(1500 / portTICK_RATE_MS);
    }
}

void test_update_area()
{
    // pre-condition
    epd_set_rotation(EPD_ROT_PORTRAIT);

    TaskHandle_t task;
    xTaskCreate(&run_test, "UpdateArea", 4096, NULL, tskIDLE_PRIORITY, &task);
}

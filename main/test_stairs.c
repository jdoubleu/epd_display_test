#include "display.h"

void print_stairs()
{
    uint8_t* fb = epd_hl_get_framebuffer(&g_display);

    for(int y = 0, x = 0; y < epd_rotated_display_height(); y++) {
        epd_draw_hline(x, y, 100, 0x00, fb);
        x += 3;
        if(x + 100 > epd_rotated_display_width())
            x = 0;
    }

    display_draw_full();
}

void test_stairs()
{
    // pre-condition
    epd_set_rotation(EPD_ROT_PORTRAIT);

    print_stairs();
}

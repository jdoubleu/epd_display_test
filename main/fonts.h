#pragma once
#include "epd_driver.h"

#ifdef INCLUDE_FONTS
#include "JetBrainsMonoBold48.h"
#include "JetBrainsMonoThin8.h"
#else
extern const EpdFont JetBrainsMonoBold48;
extern const EpdFont JetBrainsMonoThin8;
#endif

#include "display.h"

// from test_update_area.c
void test_update_area();
// from test_rotated_update_0.c
void test_rotated_update_0();
// from test_stairs.c
void test_stairs();
// from test_grid.c
void test_grid();

void app_main(void)
{
    display_setup();
    display_fullclear();

//    test_grid();

    test_rotated_update_0();
    test_update_area();
//    test_stairs();
}

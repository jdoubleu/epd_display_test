#include "display.h"

#include <esp_log.h>

static const char* TAG = "Display";
EpdiyHighlevelState g_display;

void display_setup()
{
    epd_init(EPD_LUT_64K);
    g_display = epd_hl_init(EPD_BUILTIN_WAVEFORM);

    epd_set_rotation(EPD_ROT_PORTRAIT);
}

void display_fullclear()
{
    epd_poweron();
    epd_fullclear(&g_display, epd_ambient_temperature());
    epd_poweroff();
}

void display_draw(EpdRect area)
{
    epd_poweron();
    enum EpdDrawError err = epd_hl_update_area(&g_display, MODE_GL16, epd_ambient_temperature(), area);
    if (err != EPD_DRAW_SUCCESS)
        ESP_LOGE(TAG, "Error during screen update: %d", err);
    epd_poweroff();

    epd_fill_rect(area, 0xF0, epd_hl_get_framebuffer(&g_display));
}

void display_draw_full()
{
    epd_poweron();
    enum EpdDrawError err = epd_hl_update_screen(&g_display, MODE_GL16, epd_ambient_temperature());
    if (err != EPD_DRAW_SUCCESS)
        ESP_LOGE(TAG, "Error during screen update: %d", err);
    epd_poweroff();

    epd_hl_set_all_white(&g_display);
}

void draw_border_rect(EpdRect area, int border_thickness)
{
    epd_draw_rect(area, 0x00, epd_hl_get_framebuffer(&g_display));

    for (int indent = 1; indent < border_thickness; ++indent)
    {
        EpdRect indented_area;
        indented_area.x = area.x + indent;
        indented_area.y = area.y + indent;
        indented_area.width = area.width - 2 * indent;
        indented_area.height = area.height - 2 * indent;

        epd_draw_rect(indented_area, 0x00, epd_hl_get_framebuffer(&g_display));
    }
}

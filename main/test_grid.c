#include "display.h"
#include <stdio.h>

#include "fonts.h"

typedef void (*info_fn)(int,int,int,int);

static void info_index(int i, int j, int x, int y)
{
    static char label[32];

    snprintf(label, sizeof(label), "(%d,%d)", i, j);
    int r = y + JetBrainsMonoThin8.advance_y;
    int c = x + 4; // margin
    epd_write_default(&JetBrainsMonoThin8, label, &c, &r, epd_hl_get_framebuffer(&g_display));
}

static void info_height(int i, int j, int x, int y)
{
    static char label[6];

    snprintf(label, sizeof(label), "%d", y);
    int r = y + JetBrainsMonoThin8.advance_y;
    int c = x + 4; // margin
    epd_write_default(&JetBrainsMonoThin8, label, &c, &r, epd_hl_get_framebuffer(&g_display));
}

static void info_all(int i, int j, int x, int y)
{
    static char label[32];

    snprintf(label, sizeof(label), "(%d,%d)\n%dx%d", i, j, x, y);
    int r = y + JetBrainsMonoThin8.advance_y;
    int c = x + 4; // margin
    epd_write_default(&JetBrainsMonoThin8, label, &c, &r, epd_hl_get_framebuffer(&g_display));
}

static void info_none(int i , int j, int x, int y)
{
}

void print_grid(int gutter, info_fn info)
{
    const int width = epd_rotated_display_width();
    const int height = epd_rotated_display_height();

    uint8_t* const fb = epd_hl_get_framebuffer(&g_display);

    // draw lines
    for (int row = gutter; row < height; row += gutter)
        epd_draw_hline(0, row, width, 0x00, fb);

    for (int col = gutter; col < width; col += gutter)
        epd_draw_vline(col, 0, height, 0x00, fb);

    // draw info
    int i = 0, j;

    for (int row = 0; row < height; row += gutter, ++i)
    {
        j = 0;
        for (int col = 0; col < width; col += gutter, ++j)
        {
            info(i, j, col, row);
        }
    }

    display_draw_full();
}

void test_grid()
{
    // pre-condition
    epd_set_rotation(EPD_ROT_PORTRAIT);

//    print_grid(80, &info_none);
    print_grid(80, &info_all);
//    print_grid(80, &info_index);
//    print_grid(80, &info_height);
}
